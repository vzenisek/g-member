g-member plugin by Vita Zenisek
https://bitbucket.org/vzenisek/g-member/src/master/

This plugin remembers maximum and minimum Gs and stores them in 2 float DataRefs:
g-member/MaxG
g-member/MinG

g-member/ResetLoad command tois available for reset those to 1.0 G.

There is no UI available, as it is intended for use by aircraft developers to support custom g-meter models with reset button.

G load data are persisted in aircraft livery folder in text format, so that they are portable.

Feel free to use it with any aircraft model, including commertial ones.

Enjoy!
