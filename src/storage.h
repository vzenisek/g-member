#ifndef STORAGE_H
#define STORAGE_H

#include "XPLMDataAccess.h"

extern XPLMDataRef gLiveryPathDR;

void InitStorage();
void LoadGsFromLivery(float &minG, float &maxG, bool saveOldFirst);
void SaveGsForLivery(float minG, float maxG);


#endif // STORAGE_H
