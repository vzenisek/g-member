#include "storage.h"
#include "XPLMUtilities.h"
#include "XPLMPlanes.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

XPLMDataRef gLiveryPathDR = nullptr;

static char lastLiveryPath[1024];
static char currentLiveryPath[1024];

bool getCurrentAircraftPath(char* buffer, const char* appendFile)
{
	static char fileName[256];
	XPLMGetNthAircraftModel(0, fileName, buffer);
	char * file = XPLMExtractFileAndPath(buffer);
	*(file-1) = *XPLMGetDirectorySeparator();
	*file = '\0';
	strcat(buffer, appendFile);
	return true;
}

bool getStoragePath(char* filePathBuffer)
{
    static const char* filename="g-member.dat";
    int numRead = XPLMGetDatab(gLiveryPathDR, filePathBuffer, 0, 1024);
    if (numRead > 1024 - static_cast<float>((strlen(filename)+1)))
        return false; //path longer than buffer? No space for file name anyway...
    filePathBuffer[numRead] = '\0';
    if (strlen(filePathBuffer) == 0)
      return getCurrentAircraftPath(filePathBuffer, filename);
    strcat(filePathBuffer,filename);
    return true;
}

FILE* openStorage(const char* filePath, const char* mode)
{
    FILE* f = nullptr;

#ifdef WIN32
#include <windows.h>
    //We are using Native path feature, so all paths are UTF-8. For windows, we need to convert them
    static wchar_t wCurrentLiveryPath[1024];
    if (MultiByteToWideChar(CP_UTF8, 0, filePath, -1, &wCurrentLiveryPath[0], 1024) == 0) {
        XPLMDebugString("Error: g-member: Can't convert livery path to WCHAR\n");
        return nullptr;
    }
    static wchar_t wMode[20];
    if (MultiByteToWideChar(CP_UTF8, 0, mode, -1, &wMode[0], 20) == 0) {
        XPLMDebugString("Error: g-member: Can't convert mode to WCHAR\n");
        return nullptr;
    }

    f = _wfopen(wCurrentLiveryPath, wMode);

#else
    f = fopen(filePath, mode);
#endif

    return f;
}

void InitStorage()
{
    memset(&lastLiveryPath[0],0, sizeof(lastLiveryPath));
    memset(&currentLiveryPath[0], 0, sizeof(currentLiveryPath));
}

void SaveGLoad(const char* path, float minG, float maxG)
{
    if (strlen(path) == 0)
        return;
    FILE* f = openStorage(path, "wt");
    if (f == nullptr) {
        XPLMDebugString("Error: g-member: Can't open data file for write!\n");
        return;
    }
    //write data s text so that it is portable
    fprintf(f, "%.3f\n", static_cast<double>(minG));
    fprintf(f, "%.3f\n", static_cast<double>(maxG));
    fclose(f);
    XPLMDebugString("Success: g-member: G-load data saved!\n");
}

void LoadGsFromLivery(float &minG, float &maxG, bool saveOldFirst)
{
    if (saveOldFirst)
        SaveGLoad(lastLiveryPath, minG, maxG);

    minG = maxG = 1.0;
    if (!getStoragePath(currentLiveryPath)) {
        XPLMDebugString("Error: g-member: Can't convert mode to WCHAR\n");
        return;
    }
    strcpy(&lastLiveryPath[0],currentLiveryPath);

    FILE* f = openStorage(currentLiveryPath, "rt");
    if (f == nullptr) {
        XPLMDebugString("Warning: g-member: data file does not exist. Default G-load set.\n");
        return;
    }
    char buff[100];
    memset(buff, 0, 100);
    if (fgets(&buff[0], 100, f) != nullptr)
        minG = static_cast<float>(atof(buff));
    if (minG < -15)
        minG = -15;
    if (minG > 1)
        minG = 1;
    memset(buff, 0, 100);
    if (fgets(&buff[0], 100, f) != nullptr)
        maxG = static_cast<float>(atof(buff));
    if (maxG > 15)
        maxG = 15;
    if (maxG < 1)
        maxG = 1;
    fclose(f);
    XPLMDebugString("Success: g-member: G-load data loaded!\n");
}

void SaveGsForLivery(float minG, float maxG)
{
    if (!getStoragePath(currentLiveryPath)) {
        XPLMDebugString("Error: g-member: Can't convert mode to WCHAR\n");
        return;
    }
    SaveGLoad(currentLiveryPath, minG, maxG);
}
