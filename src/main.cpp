﻿ // Based on custom commands/datarefs sample https://developer.x-plane.com/code-sample/custom-command-with-custom-dataref/


#if APL
#if defined(__MACH__)
#include <Carbon/Carbon.h>
#endif
#endif

#ifndef XPLM210
  #error This pluin requires the v2.1 SDK or newer
#endif
 
 #include "XPLMPlugin.h"
 #include "XPLMUtilities.h"
 #include "XPLMDataAccess.h"
 #include "XPLMProcessing.h"

 #include <string.h>
 #include <stdio.h>
 #include <stdlib.h>

#include "storage.h"
 
 static XPLMDataRef gMaxDR = nullptr;  //maximum g-load dataref
 static XPLMDataRef gMinDR = nullptr;  //minimum g-load dataref
 static XPLMDataRef gLoadNormalDR = nullptr;
 
 //dataref values. Initial is 1.0
 static float gMax = 1.0;
 static float gMin = 1.0;
 static bool  gOnApt = false;

 
 static float     GetMaxDRCB(void* inRefcon);
 static float     GetMinDRCB(void* inRefcon);
 
 static XPLMCommandRef resetCommand = nullptr;	//  Our two custom commands
 
 
 static int    resetCommandHandler(XPLMCommandRef       inCommand,          //  Our two custom command handlers
                                XPLMCommandPhase     inPhase,
                                void *               inRefcon);
								
static float	gLoadFLCB(float elapsedMe, float elapsedSim, int counter, void * refcon);
 
 
 PLUGIN_API int XPluginStart(
         char *        outName,
         char *        outSig,
         char *        outDesc)
 {
     XPLMEnableFeature("XPLM_USE_NATIVE_PATHS", 1);
     InitStorage();
     gOnApt = false;
	// Plugin Info
     strcpy(outName, "g-member");
     strcpy(outSig, "vitazenisek.plugins.g-member");
     strcpy(outDesc, "This plugin remembers and provides two new datarefs for min and max g-load achieved and\n custom command for reset.");
 
	 //  Create gmax load dataref
     gMaxDR = XPLMRegisterDataAccessor("g-member/MaxG",
												 xplmType_Float,                                // The types we support
												 0,                                             // Writable
                                                 nullptr, nullptr,      								// Integer accessors
                                                 GetMaxDRCB, nullptr,                              // Float accessors
                                                 nullptr, nullptr,                                    // Doubles accessors
                                                 nullptr, nullptr,                                    // Int array accessors
                                                 nullptr, nullptr,                                    // Float array accessors
                                                 nullptr, nullptr,                                    // Raw data accessors
                                                 nullptr, nullptr);                                   // Refcons not used
												 
	//  Create gmin load dataref
     gMinDR = XPLMRegisterDataAccessor("g-member/MinG",
												 xplmType_Float,                                // The types we support
												 0,                                             // Writable
                                                 nullptr, nullptr,      								// Integer accessors
                                                 GetMinDRCB, nullptr,                              // Float accessors
                                                 nullptr, nullptr,                                    // Doubles accessors
                                                 nullptr, nullptr,                                    // Int array accessors
                                                 nullptr, nullptr,                                    // Float array accessors
                                                 nullptr, nullptr,                                    // Raw data accessors
                                                 nullptr, nullptr);                                   // Refcons not used
												 
    gLoadNormalDR = XPLMFindDataRef("sim/flightmodel/forces/g_nrml");
    gLiveryPathDR = XPLMFindDataRef("sim/aircraft/view/acf_livery_path");
	 
	 // Create reset load command
     resetCommand = XPLMCreateCommand("g-member/ResetLoad", "Reset G loads");
	  
	// Register command handler
	 XPLMRegisterCommandHandler(resetCommand,           // in Command name
								resetCommandHandler,    // in Handler
								1,                          // Receive input before plugin windows.
                                nullptr);                // inRefcon.
								
    XPLMRegisterFlightLoopCallback(gLoadFLCB, 0.0, nullptr); //call every frame
                         
	return 1;
 }
 
  
 PLUGIN_API void     XPluginStop(void)
 { 
     XPLMUnregisterCommandHandler(resetCommand, resetCommandHandler, 0, nullptr);
     XPLMUnregisterFlightLoopCallback(gLoadFLCB, nullptr);
	 
	 XPLMUnregisterDataAccessor(gMaxDR);
	 XPLMUnregisterDataAccessor(gMinDR);
 }
  
 PLUGIN_API void XPluginDisable(void)
 {
 }
 
 PLUGIN_API int XPluginEnable(void)
 {
     return 1;
 }
 
 PLUGIN_API void XPluginReceiveMessage(XPLMPluginID    /*inFromWho*/,
                                      long             inMessage,
                                      void *           inParam)
 {
     switch (inMessage) {
     case XPLM_MSG_LIVERY_LOADED:
        //this comes too early during plane load. So check if it was positioned on Airport first
         if (inParam != 0 || !gOnApt)
             return;
         LoadGsFromLivery(gMin, gMax, true);
         break;
     case XPLM_MSG_AIRPORT_LOADED:
         gOnApt = true;
         LoadGsFromLivery(gMin, gMax, false);
         XPLMSetFlightLoopCallbackInterval(gLoadFLCB, 3.0, 1, nullptr);
         break;
     case XPLM_MSG_WILL_WRITE_PREFS:
         SaveGsForLivery(gMin, gMax);
         break;
     case XPLM_MSG_PLANE_UNLOADED:
         if (inParam != 0 || !gOnApt)
             return;
         SaveGsForLivery(gMin, gMax);
         break;
     default:
         return;
     }
 }
 
 static float GetMaxDRCB(void* /*inRefcon*/)
 {
     return gMax;
 }
 
 static float GetMinDRCB(void* /*inRefcon*/)
 {
     return gMin;
 }
 
 
 int    resetCommandHandler(XPLMCommandRef       /*inCommand*/,
                                XPLMCommandPhase     /*inPhase*/,
                                void *               /*inRefcon*/)
 {
	//reset load to 1 g
	gMax = gMin = 1.0;
	 
	// Returning 0 disables further processing by X-Plane. 
	return 0;
 }
 
 static float gLoadFLCB(float /*elapsedMe*/, float /*elapsedSim*/, int /*counter*/, void * /*refcon*/)
 {

	 //get the normal g load and set min/max
	 
     float normalLoad = XPLMGetDataf(gLoadNormalDR);
	 if (normalLoad > gMax)
		 gMax = normalLoad;
	 
	 if (normalLoad < gMin)
		 gMin = normalLoad;
	 
	 return  -1.0;
 }
